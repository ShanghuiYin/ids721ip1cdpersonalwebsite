+++
title = "About"
path = "about"
template = "about.html"
+++

## Who am I?
My name is **Shanghui Yin**. People always have trouble pronouncing my first name, I suggest call me _Eric_ instead.

I am current a second year ECE student at Duke University.

## What's the topic?
This blog covers my findings while exploring various aspects of programming in general, with a special focus on cloud programming.
