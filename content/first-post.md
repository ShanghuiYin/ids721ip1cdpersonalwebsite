+++
title = "Hello Zola"
date = 2024-01-01

[taxonomies]
categories = ["Template"]
tags = ["rust", "zola"]
+++

The description of the first post.

<!-- more -->

# Project Page Template

## Overview

THIS is IDS721 Project: portfolio project page templates.


## Technologies Used

- Technology 1
- Technology 2

## Results

TBD
Explain the results of your project.


## Reference

1. https://www.getzola.org/
2. https://www.getzola.org/themes/


---
---


Return to [Home](/)