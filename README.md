IDS721 Spring 2024 Individual Project 1 - Continuous Delivery of Personal Website
===============

## Demo Video

[Youtube Video Here](https://youtu.be/5OJJ_gbPUSM)
&nbsp;&nbsp;![YouTube Video Views](https://img.shields.io/youtube/views/5OJJ_gbPUSM)

## Website Hosted on Netlify

[My Personal Website](https://ericblog.netlify.app/)

> Screenshot

![img.png](img.png)

## Prerequisites

In order to realise this website, you need some software pre-installed:

- [Git](https://git-scm.com/downloads), Required for version control.

- [Node](https://nodejs.org/en/download), an open-source, cross-platform JavaScript runtime environment.

- [Zola](https://github.com/getzola/zola/releases), a fast static site generator.

- an editor or integrated development environment of your choice - I use [JetBrains RustRover](https://www.jetbrains.com/rust/),
  an IDE that makes development a more productive and enjoyable experience. 

- [Netlify](https://www.netlify.com/), a platform that offers hosting and serverless backend services for web applications and static websites.


## Zola File Structure

This is the directory structure of the stand-alone site, where the theme is in the root directory:

```
/
├── content
├── sass
├── static
│   ├── css
│   ├── img
│   └── js
├── templates
└── theme.toml
└── config.toml
└── netlify.toml
└── theme.toml
```



## Usage

To use the theme, clone this repository to your `themes` directory.
It requires that you use the categories and tags taxonomies.
This can be done with the following additions to `config.toml`:
```toml
theme = "zola-clean-blog"

taxonomies = [
    {name = "categories", rss = true, paginate_by=5},
    {name = "tags", rss = true, paginate_by=5},
]
```

## GitLab workflow to build and deploy site on push

- create a `.gitlab-ci.yml` file in the root of your repository with the content to enable the Gitlab workflow

![img_1.png](img_1.png)

## Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.

- Here I choose Netlify to host my website.
- It is a platform that offers hosting and serverless backend services for web applications and static websites.
- It is very easy to use and has a free tier that is more than enough for a personal website.
- It also has a continuous deployment feature that allows you to deploy your website automatically when you push to your repository.

![img_2.png](img_2.png)


## License

This theme is under the MIT License.


## Reference

1. https://www.getzola.org/
2. https://www.getzola.org/themes/
3. https://www.getzola.org/documentation/getting-started/overview/